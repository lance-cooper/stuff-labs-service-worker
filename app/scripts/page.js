'use strict';

// Register service worker
if ('serviceWorker' in navigator) {
	navigator.serviceWorker.register('/sw.js').then(function(registration) {
		console.log('Service worker registered with scope:', registration.scope);
	}, function(err) {
		console.log('ಠ_ಠ', err);
	});
}
