importScripts('/scripts/serviceworker-cache-polyfill.js');


var CACHE_NAME = 'v2';
var urlsToCache = [
  '/',
  '/styles/main.css',
  '/images/logo.gif',
  '/images/story-customs.jpg',
  '/images/story-gaming.jpg',
  '/images/story-large-2.jpg',
  '/images/story-large.jpg',
  '/images/story-rock.png',
  '/images/story-search.jpg',
  '/images/story-vac.jpg',
  '/fonts/bootstrap/glyphicons-halflings-regular.eot',
  '/fonts/bootstrap/glyphicons-halflings-regular.svg',
  '/fonts/bootstrap/glyphicons-halflings-regular.woff',
  '/fonts/bootstrap/glyphicons-halflings-regular.woff2',
  '/scripts/page.js',
  '/story-2.html',
  '/story.html'
];

self.addEventListener('install', function(event) {
    // Perform install steps
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function(cache) {
                console.log('Opened cache');
                return cache.addAll(urlsToCache);
            })
    );
});

self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request)
      .then(function(response) {
        // Cache hit - return response
        if (response) {
          return response;
        }

        return fetch(event.request);
      }
    )
  );
});